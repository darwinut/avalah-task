CREATE DATABASE  IF NOT EXISTS `car_shop` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `car_shop`;
-- MySQL dump 10.16  Distrib 10.1.13-MariaDB, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: car_shop
-- ------------------------------------------------------
-- Server version	10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `order_lines`
--

DROP TABLE IF EXISTS `order_lines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(5) NOT NULL,
  `product_id` varchar(5) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_id` (`order_id`),
  KEY `fk_item_id` (`product_id`),
  CONSTRAINT `fk_item_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `fk_order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_lines`
--

LOCK TABLES `order_lines` WRITE;
/*!40000 ALTER TABLE `order_lines` DISABLE KEYS */;
INSERT INTO `order_lines` VALUES (1,'O-1','P-1',2),(2,'O-1','P-2',3),(3,'O-2','P-1',4),(4,'O-2','P-2',1),(5,'O-2','P-3',2),(6,'O-2','P-4',1),(7,'O-2','P-5',3),(8,'O-3','P-1',2),(9,'O-3','P-2',1),(10,'O-3','P-3',3),(11,'O-3','P-4',1),(12,'O-3','P-5',3),(13,'O-3','P-6',1),(14,'O-3','P-7',3),(15,'O-3','P-8',2),(16,'O-3','P-9',3),(17,'O-4','P-1',1),(18,'O-4','P-2',2),(19,'O-4','P-3',3),(20,'O-4','P-4',1),(21,'O-4','P-5',3),(22,'O-4','P-6',1),(23,'O-4','P-7',2),(24,'O-4','P-8',3),(25,'O-4','P-9',1),(26,'O-4','P-10',3),(27,'O-4','P-11',1),(28,'O-4','P-12',2),(29,'O-5','P-1',3),(30,'O-5','P-2',1),(31,'O-5','P-3',1),(32,'O-5','P-4',1),(33,'O-5','P-5',1),(34,'O-5','P-6',3),(35,'O-5','P-7',2),(36,'O-5','P-8',1),(37,'O-5','P-9',3),(38,'O-5','P-10',4),(39,'O-5','P-11',1),(40,'O-5','P-12',2),(41,'O-5','P-13',2),(42,'O-5','P-14',1),(43,'O-5','P-15',3),(44,'O-5','P-16',1),(45,'O-6','P-1',4),(46,'O-6','P-2',2),(47,'O-6','P-3',1),(48,'O-6','P-4',4),(49,'O-6','P-5',2),(50,'O-6','P-6',1),(51,'O-6','P-7',1),(52,'O-6','P-8',1),(53,'O-6','P-9',1),(54,'O-6','P-10',1),(55,'O-6','P-11',1),(56,'O-6','P-12',2),(57,'O-6','P-13',1),(58,'O-6','P-14',4),(59,'O-6','P-15',1),(60,'O-6','P-16',1),(61,'O-6','P-17',2),(62,'O-6','P-18',4),(63,'O-6','P-19',1),(64,'O-6','P-20',1);
/*!40000 ALTER TABLE `order_lines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES ('O-1'),('O-2'),('O-3'),('O-4'),('O-5'),('O-6');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` varchar(5) NOT NULL,
  `name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES ('P-1','AUDI'),('P-10','MAZDA'),('P-11','MERCEDES'),('P-12','MITSUBISHI'),('P-13','OPEL'),('P-14','PEUGEOT'),('P-15','RENAULT'),('P-16','SKODA'),('P-17','SMART'),('P-18','TOYOTA'),('P-19','VOLKSWAGEN'),('P-2','BMW'),('P-20','VOLVO'),('P-3','FIAT'),('P-4','FORD'),('P-5','HONDA'),('P-6','HYUNDAI'),('P-7','JAGUAR'),('P-8','LAND_ROVER'),('P-9','LEXUS');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'car_shop'
--

--
-- Dumping routines for database 'car_shop'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-05  8:51:30

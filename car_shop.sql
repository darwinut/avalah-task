CREATE DATABASE car_shop;

USE car_shop;

CREATE TABLE Products (
 id Varchar(5) NOT NULL,
 name Varchar(10),
 PRIMARY KEY (id)
);

CREATE TABLE Orders (
 id Varchar(10) NOT NULL,
 PRIMARY KEY (id)
);

CREATE TABLE Order_lines (
 id INT AUTO_INCREMENT NOT NULL,
 order_id Varchar(5) NOT NULL,
 product_id Varchar(5) NOT NULL,
 quantity INT NOT NULL,
 PRIMARY KEY (id),
 Constraint fk_order_id foreign key (order_id) references Orders (id),
 constraint fk_item_id  foreign key (product_id) references Products (id)
);

INSERT into Products (id, name) values ('P-1','AUDI'), ('P-2','BMW'), ('P-3','FIAT'), ('P-4','FORD'), ('P-5','HONDA'), ('P-6','HYUNDAI'), ('P-7','JAGUAR'), ('P-8','LAND_ROVER'), ('P-9','LEXUS'), ('P-10','MAZDA'), ('P-11','MERCEDES'), ('P-12','MITSUBISHI'), ('P-13','OPEL'), ('P-14','PEUGEOT'), ('P-15','RENAULT'), ('P-16','SKODA'), ('P-17','SMART'), ('P-18','TOYOTA'), ('P-19','VOLKSWAGEN'), ('P-20','VOLVO');
INSERT into Orders (id) values ('O-1'), ('O-2'), ('O-3'), ('O-4'), ('O-5'), ('O-6'); 
INSERT into Order_lines (order_id, product_id, quantity) values ('O-1', 'P-1', 2), ('O-1', 'P-2', 3), ('O-2', 'P-1', 4), ('O-2', 'P-2', 1), ('O-2', 'P-3', 2), ('O-2', 'P-4', 1), ('O-2', 'P-5', 3), ('O-3', 'P-1', 2), ('O-3', 'P-2', 1), ('O-3', 'P-3', 3), ('O-3', 'P-4', 1), ('O-3', 'P-5', 3), ('O-3', 'P-6', 1), ('O-3', 'P-7', 3), ('O-3', 'P-8', 2), ('O-3', 'P-9', 3), ('O-4', 'P-1', 1), ('O-4', 'P-2', 2), ('O-4', 'P-3', 3), ('O-4', 'P-4', 1), ('O-4', 'P-5', 3), ('O-4', 'P-6', 1), ('O-4', 'P-7', 2), ('O-4', 'P-8', 3), ('O-4', 'P-9', 1), ('O-4', 'P-10', 3), ('O-4', 'P-11', 1), ('O-4', 'P-12', 2), ('O-5', 'P-1', 3), ('O-5', 'P-2', 1), ('O-5', 'P-3', 1), ('O-5', 'P-4', 1), ('O-5', 'P-5', 1), ('O-5', 'P-6', 3), ('O-5', 'P-7', 2), ('O-5', 'P-8', 1), ('O-5', 'P-9', 3), ('O-5', 'P-10', 4), ('O-5', 'P-11', 1), ('O-5', 'P-12', 2), ('O-5', 'P-13', 2), ('O-5', 'P-14', 1), ('O-5', 'P-15', 3), ('O-5', 'P-16', 1), ('O-6', 'P-1', 4), ('O-6', 'P-2', 2), ('O-6', 'P-3', 1), ('O-6', 'P-4', 4), ('O-6', 'P-5', 2), ('O-6', 'P-6', 1), ('O-6', 'P-7', 1), ('O-6', 'P-8', 1), ('O-6', 'P-9', 1), ('O-6', 'P-10', 1), ('O-6', 'P-11', 1), ('O-6', 'P-12', 2), ('O-6', 'P-13', 1), ('O-6', 'P-14', 4), ('O-6', 'P-15', 1), ('O-6', 'P-16', 1), ('O-6', 'P-17', 2), ('O-6', 'P-18', 4), ('O-6', 'P-19', 1), ('O-6', 'P-20', 1);

SELECT * FROM Products;
SELECT * FROM Orders;
SELECT * FROM Order_lines;

SELECT  product_id, Count(product_id) As Popularity 
FROM Order_lines
Where order_id IN 
  (
      Select Distinct
          order_id 
      From 
          Order_lines 
      Where 
          product_id = 'P-2'
  )   
 Group By product_id
 ORDER BY Popularity  DESC
 